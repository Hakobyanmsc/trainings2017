#include "Account.hpp"
#include <iostream>

Account::Account(int balance)
{
    setBalance(balance);
}

void
Account::setBalance(int balance)
{
    if (balance < 0) {
        balance_ = 0;
        std::cout << "Info 1: Your current balance is less than 0! We setted your balance to 0." << std::endl;  
        return;
    }
    balance_ = balance;
}

int 
Account::getBalance()
{
    return balance_;
}

int 
Account::credit(int creditAmount)
{
    balance_ = balance_ + creditAmount;
    return balance_;
}

int 
Account::debit(int debitAmount)
{
    
    if (debitAmount > balance_) {
        std::cout << "Info 2: The operation cannot be completed because debit amount exceeded account balance." << std::endl;
        return 0;
    }
    balance_ = balance_ - debitAmount;
   
    return balance_;
    
}

