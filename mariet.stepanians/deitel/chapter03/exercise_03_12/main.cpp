#include "Account.hpp"
#include <iostream>

int
main()
{
    int creditAdd = 0;
    int debitAdd = 0;
    int balanceAdd = 0;

    std::cout << "Account1: Enter your current balance: ";
    std::cin  >> balanceAdd;
    Account account1(balanceAdd);
    
    std::cout << "Account2: Enter your current balance: ";
    std::cin  >> balanceAdd;
    Account account2(balanceAdd);
    std::cout << "\nCredit proccess" << std::endl;
    
    std::cout << "      Account1: Enter the credit amount: ";
    std::cin  >> creditAdd;
    account1.credit(creditAdd);
    std::cout << account1.getBalance() << std::endl;
    
    std::cout << "      Account2: Enter the credit amount: ";
    std::cin  >> creditAdd;
    account2.credit(creditAdd);
    std::cout << account2.getBalance() << std::endl;

    std::cout << "\nDebit proccess" << std::endl;
    
    std::cout << "      Account1: Enter the debit amount: ";
    std::cin  >> debitAdd;
    account1.debit(debitAdd);
    std::cout << account1.getBalance() << std::endl;
    
    std::cout << "      Account2: Enter the debit amount: ";
    std::cin  >> debitAdd;
    account2.debit(debitAdd);
    std::cout << account2.getBalance() << std::endl;

    return 0;    

}
