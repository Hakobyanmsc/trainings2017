Fatal errors don't let the programm to run: they terminate the programm immediately. Non-fatal errors let the programm to proceed and run, but the programm may work wrong and give incorrect results. This is one of the main reasons why sometimes it is more likely to have a fatal error than a non-fatal one.

