#include <iostream>
#include <climits>

int
main()
{
    int largest1 = INT_MIN, largest2 = INT_MIN, counter = 1;
    while (counter <= 10) {
        int number;
        std::cout << "Enter number: ";
        std::cin >> number;

        if (largest2 < number) {
            if (largest1 < number) {
               largest2 = largest1;
               largest1 = number;
            } else {
               largest2 = number;
            }
        }
       
        ++counter;
    }

    std::cout << "Largest number is: " << largest1 << std::endl;
    std::cout << "Second largest number is: " << largest2 << std::endl;
    return 0;
}

