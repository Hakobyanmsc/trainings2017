#include <string>

class GradeBook
{
public:
    GradeBook(std::string name, std::string teacher);
    void setCourseName(std::string name);
    std::string getCourseName();
    void setTeacherName(std::string teacher);
    std::string getTeacherName();
    void displayMessage();
    
private:
    std::string courseName_;
    std::string teacherName_;
};
    
