#include "MiniComputer.hpp"

#include <iostream>

MiniComputer::MiniComputer(std::string name)
{
    setComputerName(name);
}

std::string
MiniComputer::getComputerName()
{
    return computerName_;
}

void
MiniComputer::setComputerName(std::string name)
{
    if (name.length() > 25) {
        computerName_ = name.substr(0, 25);
        return;
    }

    computerName_ = name;
}

int
MiniComputer::run()
{
    std::cout << getComputerName() << std::endl; /// getter
start:
    showMainMenu();
    int command = getMainCommand();
    if (0 == command) { /// exit command
        return 0;
    }
    int returnValue = executeCommand(command);
    if (0 == returnValue) { /// everything is ok
        goto start;
    }
    return returnValue;
}

void
MiniComputer::showMainMenu()
{
    std::cout << "Command Set\n"
              << "0 – exit\n"
              << "1 – load\n"
              << "2 – store\n"
              << "3 – print\n"
              << "4 – add\n\n"
              << "> Command: ";
}

int
MiniComputer::getMainCommand()
{
    int command;
    std::cin >> command;
    return command;
}

int
MiniComputer::executeCommand(int command)
{
    if (1 == command) { /// load command
        return executeLoadCommand();
    }
    if (2 == command) { /// store command
        return executeStoreCommand();
    }
    if (3 == command) { /// print command
        return executePrintCommand();
    }
    if (4 == command) { /// add command
        return executeAddCommand();
    }
    /// error case
    std::cerr << "Error 1: Command not found!" << std::endl;
    return 1;
}

int
MiniComputer::executeLoadCommand()
{
    return 0;
}

int
MiniComputer::executeStoreCommand()
{
    return 0;
}

int
MiniComputer::executeAddCommand()
{
    return 0;
}

int
MiniComputer::executePrintCommand()
{
    return 0;
}

